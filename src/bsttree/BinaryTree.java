package bsttree;

public class BinaryTree {

      Node root;
      boolean result;
     public boolean accept(int value){
       
         if(root==null)
         {
            root=new Node(value);
            //call root method
         }
         else{
                     
          
        
        // root=new Node(value);
        // root.accept(root,value);
         if(root.accept(root,value)== true){
             result= true;
         }
         else{
             result= false;
         }
         }
   
         System.out.println("Adding "+value+" :");
      System.out.println("Tree depth= "+  treeDepth(root));
       
      System.out.println("Depth of "+value+"= "+depth( root, value));
    
        trvarseTree(root);
         System.out.println("---"); 

         return result;
     }
     
     
       public void trvarseTree(Node node){
       
       if(node != null){ 
           System.out.println(""+node.data);
           trvarseTree(node.right); 
           trvarseTree(node.left);     
       }
     
       }
       
         public int treeDepth(Node node) {
        
       if(node == null){return 0;}
       else{ 
          int countRight=treeDepth(node.right);
          int countLeft=treeDepth(node.left);
          if(countRight>countLeft){
             
              //System.out.println("         tree depth right ="+countRight++);//+1 for the root 
              countRight++;
              return countRight;
         
         }
         else{
           //System.out.println("            tree depth left ="+countLeft++);//+1 for the root 
           countLeft++;
          return countLeft;//+1 for the root
         }
       
       }

      }

    public int depth(Node node,int value) {
          return depthHelper(node,value,1);
    }
    
     public int depthHelper(Node node,int value,int level) {
         if(node ==null) return 0;//no tree
         if(node.data==value)return level;//tree with root only
         int nextLevel = depthHelper(node.left, value, level+1);
         if(nextLevel !=0) return nextLevel;
         nextLevel = depthHelper(node.right, value, level+1);
         return nextLevel;
         
     }
    
    
 
    
}
